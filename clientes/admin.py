# -*- coding: utf-8 -*-
from django.contrib import admin

from import_export.admin import ImportExportMixin

from .models import (
    Cliente,
    ClienteDocumento,
)
from .admin_resources import ClientInyectorModelResource


@admin.register(Cliente)
class ClienteAdmin(ImportExportMixin, admin.ModelAdmin):
    '''
    Configuración Django admin e importación de datos.
    Formatos soportados:
        -CSV
        -XLS
        -XLSX
        -TSV
        -JSON
        -YAML
    '''

    resource_class = ClientInyectorModelResource
    search_fields = (
        'codigo',
        'nombre',
        'tipo',
    )
    readonly_fields = (
        'created',
        'modified'
    )
    list_display = (
        'codigo', 
        'nombre', 
        'tipo',
        'created',
        'user_create', 
        'is_active',
    )


@admin.register(ClienteDocumento)
class ClienteDocumentoAdmin(admin.ModelAdmin):
    search_fields = (
        'documento',
    )
    list_display = (
        'documento',
        'user_create',
        'created',
        'modified',
        'is_active'
    )
    readonly_fields = (
        'created',
        'modified'
    )