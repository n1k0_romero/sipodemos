# -*- coding: utf-8 -*-
import os.path
import xlrd

from django.conf import settings
from django.contrib import messages
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    FormView,
)

from braces.views import GroupRequiredMixin

from core.view_mixins import CreateMixin
from djangosearchpaginationplus.views import (
    DinamicPaginationMixin, 
    SearchMixin
)
from .constants import IMPORT_CLIENTS_INDICATION
from .forms import (
    ClienteForm,
    ClienteImportlsXForm,
)
from .models import Cliente


class  ClienteListView(GroupRequiredMixin, DinamicPaginationMixin,
                        SearchMixin, ListView):
    group_required = ('administrador_front', )
    model = Cliente
    template_name = 'clientes/list.html'

    def get_filter(self, queryset):
        search = self.get_search()

        if search:
            queryset = queryset.filter(
                Q(nombre__icontains=search) |
                Q(codigo__icontains=search) 
            )
        return queryset


class ClienteDetailView(GroupRequiredMixin, DetailView):
    group_required = ('administrador_front', )
    model = Cliente
    template_name = 'clientes/detail.html'


class ClienteCreateView(CreateMixin, CreateView):
    form_class = ClienteForm
    template_name = 'core/form.html'
    title = 'Crear Cliente'
    button_legend = 'Crear Nuevo Cliente'
    success_msg = 'Cliente creado con exito'


class ClientesImportlsXView(GroupRequiredMixin, FormView):
    group_required = ('administrador_cuentas', )
    raise_exception = True
    form_class = ClienteImportlsXForm
    template_name = 'core/form.html'
    success_url = reverse_lazy('clientes:listar')
    HEADERS = [
        'codigo',
        'nombre',
    ]
    COL_CODIGO = 0
    COL_NOMBRE = 1
    errors = list()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        messages.warning(self.request, IMPORT_CLIENTS_INDICATION)
        data = {
            'title': 'Importar Clientes desde XLSX',
            'button_legend': 'Importar Clientes desde Excel'
        }
        context.update(data)
        return context

    def handle_temporary_file(self, file):
        """Manejo de archivo temporal."""

        mediapath = os.path.join(settings.MEDIA_ROOT, 'documentos_clientes')
        if not os.path.exists(mediapath):
            os.makedirs(mediapath)
        filepath = os.path.join(mediapath, "tmp_%s" % file.name)
        with open(filepath, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)
        return filepath

    def is_valid_file(self, file):
        """Verifica si es un archivo valido."""

        wb = xlrd.open_workbook(file, encoding_override="utf-8")
        sh = wb.sheet_by_index(0)
        self.errors.clear()

        # validar emcabezados
        headers = sh.row_values(0)
        if headers != self.HEADERS:
            self.errors.append(HEADERS_ERROR)

        # validar grupo_codigo
        codigo_empty = False
        codigo_length_fail = False
        codigo_alphanumeric_fail = False

        for row in range(1, sh.nrows):
            codigo = sh.cell_value(row, self.COL_CODIGO).upper()
            # codigo empty
            if codigo == '':
                codigo_empty = True
                self.errors.append(
                    'Error codigo vacío, revisar error en fila %s' % (row + 1)
                )
                break
            # codigo length
            if len(codigo) != 7:
                codigo_length_fail = True
                self.errors.append(
                    'Error código en fila %s (revisión de longitud)' % (row + 1)
                )
                break
            # codigo not alphanumeric
            if not codigo.isalnum():
                codigo_alphanumeric_fail = True
                self.errors.append(
                    'Error codigo en fila %s, caracter no permitido' % (row + 1)
                )
                break
        return (headers == self.HEADERS and 
                not codigo_empty and  
                not codigo_length_fail and  
                not codigo_alphanumeric_fail)
                
    def form_valid(self, form):
        file = self.handle_temporary_file(form.cleaned_data['documento'])
        if self.is_valid_file(file):
            os.remove(file)
            form.instance.user_create = self.request.user
            clients_document = form.save()
            result = clients_document.cargar_clientes()
            if result['errors']:
                clients_document.documento.delete()
                clients_document.delete()
                messages.error(
                    self.request, 
                    'Fallo indefinido, contacte al adminstrador.'
                )
            else:
                messages.success(
                    self.request,
                    '%s Clientes han sido cargados exitosamente.' 
                    %(result['uploaded'])
                )
        # levantar errores.
        else:
            os.remove(file)
            for error in self.errors:
                messages.error(self.request, error)
            return super().form_invalid(form)
        return super().form_valid(form)