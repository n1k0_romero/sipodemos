# -*- coding: utf-8 -*-
from .models import (
    Cliente,
    ClienteDocumento,
)
from django import forms 


class ClienteForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Cliente
        fields = [
            'nombre',
            'codigo',
            'tipo',
        ]


class ClienteImportlsXForm(forms.ModelForm):

    class Meta:
        model = ClienteDocumento
        fields = ('documento',)


