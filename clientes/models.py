# -*- coding: utf-8 -*-
import uuid
import xlrd

from django.db import (
    models,
    transaction, 
    IntegrityError
)
from django.urls import reverse

from core.behaviors import (
    BasicTimeStampable,
    Nameable,
)
from .behaviors import ClientCodeable


class Cliente(BasicTimeStampable, Nameable, ClientCodeable):
    '''Administra la información de los grupos.'''

    TIPO_REGULAR = 'REGULAR'
    TIPO_LIDER = 'LIDER'

    TIPO_OPCIONES = (
        (TIPO_REGULAR, 'CLIENTE REGULAR'),
        (TIPO_LIDER, 'CLIENTE LIDER'),
    )

    id = models.UUIDField(
        primary_key=True, 
        default=uuid.uuid4, 
        editable=False
    )
    tipo = models.CharField(
        max_length=10,
        choices=TIPO_OPCIONES,
        default=TIPO_REGULAR,
        help_text="Tipo de cliente"
    )

    def __str__(self):
        return '%s - %s' % (self.codigo, self.nombre)

    def get_absolute_url(self):
        return reverse("clientes:detalle", kwargs={"pk": self.pk})

    class Meta:
         ordering = ['nombre']
         verbose_name = "Cliente"
         verbose_name_plural = "Clientes"


class ClienteDocumento(BasicTimeStampable):
    """
    Administra la información de archivos 
    de carga acerca de las cuentas.
    """

    documento = models.FileField(
        upload_to='documentos_clientes',
        help_text='Archivo XLSX para agregar clientes'
    )

    def cargar_clientes(self):
        '''
        Recupera el archivo xlsx previamente validado
        e inserta los clientes correspondientes.
        '''

        wb = xlrd.open_workbook(
            self.documento.path, 
            encoding_override="utf-8"
        )
        sh = wb.sheet_by_index(0)

        uploaded = 0
        errors = []
        COL_CODIGO = 0
        COL_NOMBRE = 1

        try:
            with transaction.atomic():
                if sh.nrows > 1:
                    for row in range(1, sh.nrows):
                        try:
                            values = sh.row_values(row)
                            Cliente.objects.create(
                                codigo=values[COL_CODIGO],
                                nombre=values[COL_NOMBRE],
                                user_create=self.user_create
                            )
                            uploaded += 1
                        except Exception as e:
                            errors.append(e)
                            continue
            if errors:
                raise IntegrityError
        except IntegrityError:
            pass
        return {
            'uploaded': uploaded,
            'errors': errors
            }
    
    class Meta:
        ordering = ['-created']
        verbose_name = "Documento de cliente"
        verbose_name_plural = "Documentos de clientes"