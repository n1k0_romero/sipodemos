# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.test import TestCase

from .models import Cliente


class ClienteTestCase(TestCase):

	def setUp(self):
		'''Inicializa recursos necesarios.'''

		self.test_user1 = User.objects.create_user(
			username='test_user1',
			password='abc123'
		)		

		self.test_cliente = Cliente.objects.create(
			user_create = self.test_user1,
			nombre = 'Cliente TEST',
			codigo = '1234567'
		)

	def test_cliente_user_create(self):
		'''Verifica que el cliente tenga un usuario creador.'''

		user_create_esperado = f'{self.test_cliente.user_create}'
		self.assertEqual(user_create_esperado, 'test_user1')	

	def test_cliente_esta_activo(self):
		'''Verifica que el cliente este activo.'''

		activo_esperado = self.test_cliente.is_active
		self.assertEqual(activo_esperado, True)

	def test_cliente_nombre(self):
		'''Verifica el nombre del cliente.'''

		nombre_esperado = f'{self.test_cliente.nombre}'
		self.assertEqual(nombre_esperado, 'Cliente TEST')


	def test_cliente_codigo(self):
		'''Verifica el codigo del cliente.'''

		codigo_esperado = f'{self.test_cliente.codigo}'
		self.assertEqual(codigo_esperado, '1234567')	

	def test_cliente_default_tipo_regular(self):
		'''Verifica el tipo del cliente por default.'''

		tipo_esperado = f'{self.test_cliente.tipo}'
		self.assertEqual(tipo_esperado, Cliente.TIPO_REGULAR)