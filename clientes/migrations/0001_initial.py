# Generated by Django 2.2.12 on 2020-05-21 19:18

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True, help_text='Is the record active?')),
                ('nombre', models.CharField(help_text='Nombre', max_length=100, validators=[django.core.validators.RegexValidator(code='SRT_ONLY_LETTERS', message='Error... only leters are allowed in this field', regex='^[ a-zA-ZñáéíóúÑÁÉÍÓÚ ]+$')])),
                ('codigo', models.CharField(help_text='Codigo interno de indentificación', max_length=7, unique=True, validators=[django.core.validators.RegexValidator(code='CHAR_CLIENT_CODE', message='Error... client code are made up of 7 characters', regex='^[ A-Za-z0-9 ]+$')])),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('user_create', models.ForeignKey(help_text='User who creates the record', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Cliente',
                'verbose_name_plural': 'Clientes',
                'ordering': ['nombre'],
            },
        ),
    ]
