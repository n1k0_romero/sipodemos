from django.urls import path, include

from .views import(
	ClienteListView,
	ClienteDetailView,
	ClienteCreateView,
	ClientesImportlsXView,
)


clientes_patterns = ([
	path('listar/', ClienteListView.as_view(), name='listar'),
	path('<slug:pk>/detalle/', ClienteDetailView.as_view(), name='detalle'),
	path('crear/', ClienteCreateView.as_view(), name='crear'),
	path('importar/', ClientesImportlsXView.as_view(), name='importar'),
	
	# API
    path('api/', include('clientes.api.urls')),
], 'clientes')