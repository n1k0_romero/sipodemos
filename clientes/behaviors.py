# -*- coding: utf-8 -*-
from django.core.validators import RegexValidator
from django.db import models
from core.regex import(
    REGEX_CODE,
    ONLY_CODE_MESSAGE,
    CODE_CODE 
)


class ClientCodeable(models.Model):
    """
    Clase abstracta que provee el codigo del cliente.
    """

    codigo = models.CharField(
        unique=True,
    	max_length=7,
        help_text='Código de indentificación de cliente',
        validators=[
        	RegexValidator(
        		regex=REGEX_CODE,
        		message=ONLY_CODE_MESSAGE,
        		code=CODE_CODE
    		)
        ]
    )

    class Meta:
        abstract = True