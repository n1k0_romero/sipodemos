# -*- coding: utf-8 -*-
from core.admin_mixins import ModelResourceSetUser

from .models import Cliente


class ClientInyectorModelResource(ModelResourceSetUser):
    '''
    Recurso para la importación de datos del cliente.
    Formatos soportados:
        -CSV
        -XLS
        -XLSX
        -TSV
        -JSON
        -YAML
    '''

    class Meta:
        model = Cliente



