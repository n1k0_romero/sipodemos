from django.urls import path
from .views import (
	ClienteListAPIView,
	ClienteDetailAPIView
)


urlpatterns = [
	path('', ClienteListAPIView.as_view(), name='list'),
	path('<slug:pk>/', ClienteDetailAPIView.as_view(), name='detail'),
]