from rest_framework import generics

from ..models import Cliente
from .serializers import ClienteSerializer


class ClienteListAPIView(generics.ListAPIView):
	queryset = Cliente.objects.all()
	serializer_class = ClienteSerializer


class ClienteDetailAPIView(generics.RetrieveAPIView):
	queryset = Cliente.objects.all()
	serializer_class = ClienteSerializer