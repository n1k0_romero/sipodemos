from rest_framework import serializers

from ..models import Cliente


class ClienteSerializer(serializers.ModelSerializer):
	user_create_name = serializers.CharField(source='user_create.username', read_only=True)

	class Meta:
		model = Cliente
		fields = (
			'id',
			'nombre',
			'codigo',
			'is_active',
			'user_create_name',
			'created',
			'modified',
		)