from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from core.urls import core_patterns
from clientes.urls import clientes_patterns
from cuentas.urls import cuentas_patterns
from grupos.urls import grupos_patterns
from pagos.urls import pagos_patterns


urlpatterns = [
	# Django Manager path
    path('caronte/', admin.site.urls),
    # Auth paths
    path('accounts/', include('django.contrib.auth.urls')),

    # own apps path 
    path('', include(core_patterns)),
    path('clientes/', include(clientes_patterns)),
    path('cuentas/', include(cuentas_patterns)),
    path('grupos/', include(grupos_patterns)),
    path('pagos/', include(pagos_patterns)),
]


# local configuration
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns