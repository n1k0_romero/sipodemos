import os
from django.core.wsgi import get_wsgi_application


environment = os.environ.get('PROJECT_ENV', 'development')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sipodemos.settings.%s' % environment)

application = get_wsgi_application()
