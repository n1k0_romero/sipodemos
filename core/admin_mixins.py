# -*- coding: utf-8 -*-
from import_export import resources


class ModelResourceSetUser(resources.ModelResource):
    '''
    Recurso para la importación de datos.
	Agrega el usuario que realizo la acción.
    '''

    def before_import_row(self, row, **kwargs):
        '''
        Recupera el usuario que esta haciendo
        la importación de los datos.
        '''
        
        row['user_create'] = kwargs['user'].id