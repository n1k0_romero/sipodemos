# -*- coding: utf-8 -*-


REGEX_ONLY_LETTERS = '^[ a-zA-ZñáéíóúÑÁÉÍÓÚ ]+$'
ONLY_LETTERS_MESSAGE = 'Error... only leters are allowed in this field'
CODE_ONLY_LETTERS = 'SRT_ONLY_LETTERS'


REGEX_CODE = '^[ A-Za-z0-9 ]+$'
ONLY_CODE_MESSAGE = 'Error... code are made up of 7 characters'
CODE_CODE = 'CHAR_CLIENT_CODE'