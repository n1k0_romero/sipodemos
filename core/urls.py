from django.urls import path, include

from .views import IndexTemplateView


core_patterns = ([
	path('', IndexTemplateView.as_view(), name='index'),
], 'core')