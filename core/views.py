# -*- coding: utf-8 -*-
from django.views.generic import TemplateView

from braces.views import GroupRequiredMixin


class IndexTemplateView(GroupRequiredMixin, TemplateView):
    group_required = ('administrador_front', )
    template_name = 'core/index.html'

