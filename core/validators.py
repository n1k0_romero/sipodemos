# -*- coding: utf-8 -*-
from django.utils import timezone


def validate_date(date):
    '''
    Valida que la fecha no sea anterior
    al dia de hoy.
    '''
    
    if date < timezone.now().date():
        raise ValidationError(
            "Error no puedes ingresar una fecha anterior a hoy"
        )
