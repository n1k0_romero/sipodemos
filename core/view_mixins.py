# -*- coding: utf-8 -*-
from django.contrib import messages


class CreateMixin:
    '''
    Mixin con funcionalidad base
    de Create generico
    '''

    @property
    def success_msg(self):
        return NotImplemented

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = {
            'title': self.title,
            'button_legend': self.button_legend,
        }
        context.update(data)
        return context
    
    def form_valid(self, form):
        form.instance.user_create = self.request.user
        form.save()
        messages.success(
            self.request, 
            self.success_msg,
        )
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.get_absolute_url()