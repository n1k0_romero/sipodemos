# -*- coding: utf-8 -*-# 
from django.core.validators import RegexValidator
from django.contrib.auth.models import User
from django.db import models
from .regex import (
    REGEX_ONLY_LETTERS,
    ONLY_LETTERS_MESSAGE,
    CODE_ONLY_LETTERS, 
) 


class TimeStampable(models.Model):
    """
    An abstract base class model that provides self-updating
    ``created`` and ``modified`` fields.
    """

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class UserCreateable(models.Model):
    """
    An abstract base class model that provides
    the user who saves the record.
    """

    user_create = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        help_text='User who creates the record'
    )

    class Meta:
        abstract = True


class IsActiveable(models.Model):
    """
    An abstract base class model that indicates
    whether the record is active.
    """
    
    is_active = models.BooleanField(
        default=True,
        help_text='Is the record active?'
    )

    class Meta:
        abstract = True


class BasicTimeStampable(TimeStampable, 
                         UserCreateable, 
                         IsActiveable):
    """
    An abstract base class model that indicates
    the basic time stamp.

    this class registers:
        - created
        - modified
        - user_create
        - is_active
    """

    class Meta:
        abstract = True


class Nameable(models.Model):
    """An abstract base class model of a name"""

    nombre = models.CharField(
        max_length=100,
        help_text="Nombre",
        validators=[
            RegexValidator(
                regex=REGEX_ONLY_LETTERS,
                message=ONLY_LETTERS_MESSAGE,
                code=CODE_ONLY_LETTERS
            )
        ]
    )

    class Meta:
        abstract = True