# -*- coding: utf-8 -*-
from random import randint


def random_with_N_digits(n):
    '''
    Recibe una longitud como parametro
    y regresa esa cantidad de numeros
    aleatorios.
    
    ejemplo.
    random_with_N_digits(8)
    >>>70263394
    '''
    
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(
        range_start, 
        range_end
    )

