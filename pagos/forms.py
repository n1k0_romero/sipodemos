# -*- coding: utf-8 -*-
from django import forms 

from .constants import (
    PAY_ERROR_MESSAGE,
    CODE_PAY_ERROR
)
from .models import Abono


class AbonoCreateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.pago_individual = kwargs.pop('pago_individual')
        # form_control class dinamico a los fields
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
        # monto para liquidar sugerido de inicio 
        self.fields['monto'].initial = self.pago_individual.calcular_monto_liquidar()

    class Meta:
        model = Abono
        fields = [
            'monto',
        ]

    def clean_monto(self):
        cleaned_data = super().clean()
        monto = cleaned_data.get('monto')
        if monto >  self.pago_individual.calcular_monto_liquidar():
            raise forms.ValidationError(
                PAY_ERROR_MESSAGE,
                CODE_PAY_ERROR
            )
        return monto