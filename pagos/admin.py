# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import (
    Calendario,
    PagoIndividual,
    Abono,
)


@admin.register(Calendario)
class CalendarioAdmin(admin.ModelAdmin):
    readonly_fields = (
        'created',
        'modified'
    )
    list_display = (
        'id',
        'numero_pagos',
        'created',
    )


@admin.register(PagoIndividual)
class PagoIndividualAdmin(admin.ModelAdmin):
    search_fields = (
        'estatus',
        'fecha',
    )
    readonly_fields = (
    	'monto_individual',
    	'fecha',
        'estatus',
        'created',
        'modified'
    )
    list_display = (
    	'monto_individual',
    	'fecha',
        'estatus',
        'created',
        'modified',
        'user_create', 
        'is_active',
    )


@admin.register(Abono)
class AbonoAdmin(admin.ModelAdmin):
    readonly_fields = (
        'created',
        'modified'
    )
    list_display = (
        'id',
        'monto',
        'created',
    )