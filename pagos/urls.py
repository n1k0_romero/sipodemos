from django.urls import path
from .views import AbonoCreateView


pagos_patterns = ([
	path(
		'crear/pago/<slug:pk_pago>/cuenta/<slug:pk_cuenta>/', 
		AbonoCreateView.as_view(), 
		name='crear'
	),
], 'pagos')