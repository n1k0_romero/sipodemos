# -*- coding: utf-8 -*-
from datetime import date

from django.contrib.auth.models import User
from django.test import TestCase

from .models import (
	Calendario,
	PagoIndividual,
	Abono,
)


class CalendarioTestCase(TestCase):

	def setUp(self):
		'''Inicializa recursos necesarios.'''

		self.test_user1 = User.objects.create_user(
			username='test_user1',
			password='abc123'
		)		

		self.test_calendario = Calendario.objects.create(
			user_create = self.test_user1,
			numero_pagos = 5
		)

	def test_calendario_user_create(self):
		'''Verifica que el calendario tenga un usuario creador.'''

		user_create_esperado = f'{self.test_calendario.user_create}'
		self.assertEqual(user_create_esperado, 'test_user1')

	def test_calendario_esta_activo(self):
		'''Verifica que el calendario este activo.'''

		activo_esperado = self.test_calendario.is_active
		self.assertEqual(activo_esperado, True)

	def test_calendario_numero_pagos(self):
		'''Verifica el numero de pagos.'''

		numero_pagos_esperado = self.test_calendario.numero_pagos
		self.assertEqual(numero_pagos_esperado, 5)	

	def test_calendario_periodo_pago(self):
		'''Verifica el calendario tenga un periodo de pago.'''

		periodo_pago_esperado = self.test_calendario.periodo_pago
		self.assertEqual(periodo_pago_esperado, Calendario.PAGO_SEMANAL)


class PagoIndividualTestCase(TestCase):

	def setUp(self):
		'''Inicializa recursos necesarios.'''

		self.test_user1 = User.objects.create_user(
			username='test_user1',
			password='abc123'
		)		

		self.test_pago_individual = PagoIndividual.objects.create(
			user_create = self.test_user1,
			monto_individual = 100,
			fecha = date.today()
		)

	def test_pago_individual_user_create(self):
		'''Verifica que el pago_individual tenga un usuario creador.'''

		user_create_esperado = f'{self.test_pago_individual.user_create}'
		self.assertEqual(user_create_esperado, 'test_user1')

	def test_pago_individual_esta_activo(self):
		'''Verifica que el pago individual este activo.'''

		activo_esperado = self.test_pago_individual.is_active
		self.assertEqual(activo_esperado, True)

	def test_pago_individual_monto_individual(self):
		'''Verifica el pago individual tenga monto.'''

		monto_individual_esperado = self.test_pago_individual.monto_individual
		self.assertEqual(monto_individual_esperado, 100)	

	def test_pago_individual_estatus(self):
		'''Verifica el pago_individual tenga estatus.'''

		pago_individual_estatus_esperado = self.test_pago_individual.estatus
		self.assertEqual(pago_individual_estatus_esperado, PagoIndividual.ESTATUS_PENDIENTE)


class AbonoTestCase(TestCase):

	def setUp(self):
		'''Inicializa recursos necesarios.'''

		self.test_user1 = User.objects.create_user(
			username='test_user1',
			password='abc123'
		)		

		self.test_abono = Abono.objects.create(
			user_create = self.test_user1,
			monto = 100,
		)

	def test_abono_user_create(self):
		'''Verifica que el abono tenga un usuario creador.'''

		user_create_esperado = f'{self.test_abono.user_create}'
		self.assertEqual(user_create_esperado, 'test_user1')

	def test_abono_esta_activo(self):
		'''Verifica que el abono este activo.'''

		activo_esperado = self.test_abono.is_active
		self.assertEqual(activo_esperado, True)


	def test_abono_monto(self):
		'''Verifica el abono tenga monto.'''

		monto_esperado = self.test_abono.monto
		self.assertEqual(monto_esperado, 100)	

