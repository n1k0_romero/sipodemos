# -*- coding: utf-8 -*-
import uuid
from datetime import date

from django.core.validators import MinValueValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from core.behaviors import (
    TimeStampable,
    BasicTimeStampable
)
from core.validators import validate_date


class Calendario(BasicTimeStampable):
    '''
    Administra la información del calendario de pagos.
    Al crearse una cuenta se crea un calendario de pagos.
    El monto del la cuenta se divide entre el numero 
    de pagos individuales que tendra.
    '''

    PAGO_SEMANAL = 7
    PAGO_QUINCENAL = 15
    PAGO_MENSUAL = 30

    PERIDO_PAGO_OPCIONES = (
        (PAGO_SEMANAL, 'SEMANAL'),
        (PAGO_QUINCENAL, 'QUINCENAL'),
        (PAGO_MENSUAL, 'MENSUAL'),
    )

    id = models.UUIDField(
        primary_key=True, 
        default=uuid.uuid4, 
        editable=False
    )
    numero_pagos = models.PositiveSmallIntegerField()
    pagos = models.ManyToManyField(
        'PagoIndividual',
        related_name='calendario',
        help_text="Pagos individuales del calendario"
    )
    periodo_pago = models.PositiveSmallIntegerField(
        choices=PERIDO_PAGO_OPCIONES,
        default=PAGO_SEMANAL,
        help_text='Periodo de pago'
    )

    @property
    def monto_individual(self):
        cuenta = self.cuenta.all().first()
        monto_individual = cuenta.monto / self.numero_pagos
        return monto_individual
    
    @classmethod
    def crear(cls, numero_pagos, user_create):
        '''
        Crea un nuevo calendario con base en 
        un numero de pagos y el usuario .
        '''

        calendar = cls(
            numero_pagos = numero_pagos,
            user_create = user_create
        )
        calendar.save()
        return calendar

    def __str__(self):
        return '%s' % self.id

    class Meta:
        ordering = ['-created']
        verbose_name = "Calendario"
        verbose_name_plural = "Calendarios"


class PagoIndividual(BasicTimeStampable):
    '''Administra la información de pagos individuales.'''

    ESTATUS_PENDIENTE = 'PENDIENTE'
    ESTATUS_PARCIAL = 'PARCIAL'
    ESTATUS_PAGADO = 'PAGADO'

    ESTATUS_OPCIONES = (
    	(ESTATUS_PENDIENTE, 'Pago PENDIENTE'),
    	(ESTATUS_PARCIAL, 'Pago PARCIAL'),
    	(ESTATUS_PAGADO, 'Pago PAGADO'),
	)

    id = models.UUIDField(
        primary_key=True, 
        default=uuid.uuid4, 
        editable=False
    )
    monto_individual = models.DecimalField(
    	max_digits=15,
    	decimal_places=2,
        validators=[
            MinValueValidator(0)
        ],
        help_text='Monto individual del pago'
    )  
    fecha = models.DateField(
    	help_text='Fecha esperada de pago',
        validators=[validate_date],
	)
    estatus = models.CharField(
        max_length=12,
        choices=ESTATUS_OPCIONES,
        default=ESTATUS_PENDIENTE,
        help_text="Estatus del pago"
    )
    abonos = models.ManyToManyField(
        'Abono',
        related_name='pago_individual',
        help_text="Abonos a pagos individuales"
    )

    @classmethod
    def crear(cls, monto_individual, fecha, user_create):
        individual_payment = cls(
            monto_individual = monto_individual,
            fecha = fecha,
            user_create = user_create
        )
        individual_payment.save()
        return individual_payment

    @property
    def esta_atrasado(self):
        if date.today() > self.fecha:
            return True
        return False

    def calcular_monto_abonos(self):
        abonos = 0
        for abono in self.abonos.all():
            abonos += abono.monto
        return abonos     

    def calcular_numero_abonos(self):
        return self.abonos.all().count()

    def calcular_monto_liquidar(self):
        abonos = self.calcular_monto_abonos()
        return self.monto_individual - abonos

    def abono_bloqueado(self):
        monto_liquidar = self.calcular_monto_liquidar()
        if int(monto_liquidar) == 0:
            return True
        return False

    def __str__(self):
        return '%s - %s - %s' % (
        	self.fecha, 
        	self.monto_individual, 
        	self.estatus
    	)

    class Meta:
        ordering = ['created']
        verbose_name = "Pago Individual"
        verbose_name_plural = "Pagos Individuales"


class Abono(BasicTimeStampable):
    '''
    Administra la información de 
    abonos a pagos indivuales.
    '''

    id = models.UUIDField(
        primary_key=True, 
        default=uuid.uuid4, 
        editable=False
    )
    monto = models.DecimalField(
        max_digits=15,
        decimal_places=2,
        validators=[
            MinValueValidator(1)
        ],
        help_text='Monto del abono'
    )

    def __str__(self):
        return str(self.monto)

    class Meta:
        ordering = ['created']
        verbose_name = "Abono"
        verbose_name_plural = "Abonos"


