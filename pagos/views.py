# -*- coding: utf-8 -*-
from django.contrib import messages
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView

from braces.views import GroupRequiredMixin

from cuentas.models import Cuenta
from pagos.models import PagoIndividual
from .constants import CREATE_PAY_INDICATION
from .forms import AbonoCreateForm
from .models import Abono
from .utils import actualizar_estatus_pago_individual
 

class AbonoCreateView(GroupRequiredMixin, CreateView):
    form_class = AbonoCreateForm
    group_required = ('administrador_front', )
    template_name = 'pagos/abono_form.html'
    success_url = reverse_lazy('cuentas:listar')
    title = 'Crear abono'
    success_msg = 'Gracias por su pago'

    def get_form_kwargs(self):
        """Inyecta al formulario los kwargs."""

        kwargs = super().get_form_kwargs()
        kwargs['pago_individual'] = self.get_pago_individual()
        return kwargs

    def get_pago_individual(self):
        '''
        Recupera el abono individual 
        al cual se vincula el abono
        '''

        pago_individual = get_object_or_404(
            PagoIndividual, 
            pk=self.kwargs.get('pk_pago')
        )
        return pago_individual


    def get_cuenta(self):
        '''Recupera la cuenta.'''

        cuenta = get_object_or_404(
            Cuenta, 
            pk=self.kwargs.get('pk_cuenta')
        )
        return cuenta

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        messages.success(self.request, CREATE_PAY_INDICATION)
        pago_individual = self.get_pago_individual()
        data = {
            'title': 'Registrar Abono',
            'button_legend': 'Registrar Abono',
            'pago_individual': pago_individual
        }
        context.update(data)
        return context

    def form_valid(self, form):
        monto = form.instance.monto
        form.instance.user_create = self.request.user
        nuevo_abono = form.save()
        # agregar abono a pago indivudual
        pago_individual = self.get_pago_individual()
        pago_individual.abonos.add(nuevo_abono)
        # actualizar estatus de abono individual
        actualizar_estatus_pago_individual(
            pago_individual = pago_individual,
            abono = nuevo_abono
        )
        # cerra cuenta si es posible
        cuenta = self.get_cuenta()
        if cuenta.puede_cerrarse:
            cuenta.cerrar()
        messages.success(
            self.request, 
            self.success_msg,
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            'cuentas:detalle', 
            kwargs = { 'pk': self.get_cuenta().pk }
        )



