from .models import PagoIndividual


def actualizar_estatus_pago_individual(pago_individual, abono):
	'''
	Actualiza el estatus del pago individual
	dependiendo el abono recibido
	'''

	if  pago_individual.calcular_monto_liquidar() == 0:
		pago_individual.estatus = PagoIndividual.ESTATUS_PAGADO
		pago_individual.save()
		
	else:
		pago_individual.estatus = PagoIndividual.ESTATUS_PARCIAL
		pago_individual.save()
	return pago_individual
