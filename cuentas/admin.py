# -*- coding: utf-8 -*-
from django.contrib import admin

from import_export.admin import ImportExportMixin

from .models import Cuenta


@admin.register(Cuenta)
class CuentaAdmin(admin.ModelAdmin):
    '''
    Importación formatos soportados:
        -CSV
        -XLS
        -XLSX
        -TSV
        -JSON
        -YAML
    '''

    search_fields = (
        'grupo__name',
    )
    readonly_fields = (
        'codigo',
        'created',
        'modified'
    )
    list_display = (
        'codigo',
        'grupo', 
        'estatus', 
        'monto', 
        'created',
        'user_create', 
        'is_active',
    )

