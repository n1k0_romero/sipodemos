# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.test import TestCase

from grupos.models import Grupo
from .models import Cuenta
from pagos.models import Calendario


class CuentaTestCase(TestCase):

	def setUp(self):
		'''Inicializa recursos necesarios.'''

		self.test_user1 = User.objects.create_user(
			username='test_user1',
			password='abc123'
		)

		self.test_grupo = Grupo.objects.create(
			user_create = self.test_user1,
			nombre = 'Grupo TEST',
			codigo = '12345'
		)		

		self.test_calendario = Calendario.objects.create(
			user_create = self.test_user1,
			numero_pagos = 5
		)

		self.test_cuenta = Cuenta.objects.create(
			user_create = self.test_user1,
			grupo = self.test_grupo,
			calendario = self.test_calendario,
			monto = 500,
		)

	def test_cuenta_user_create(self):
		'''Verifica que la cuenta tenga un usuario creador.'''

		user_create_esperado = f'{self.test_cuenta.user_create}'
		self.assertEqual(user_create_esperado, 'test_user1')

	def test_cuenta_esta_activo(self):
		'''Verifica que la cuenta este activo.'''

		activo_esperado = self.test_cuenta.is_active
		self.assertEqual(activo_esperado, True)	

	def test_cuenta_default_estatus(self):
		'''Verifica el tipo de cuenta por default.'''

		estatus_esperado = f'{self.test_cuenta.estatus}'
		self.assertEqual(estatus_esperado, Cuenta.ESTATUS_DESEMBOLSADA)