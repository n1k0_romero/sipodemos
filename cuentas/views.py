# -*- coding: utf-8 -*-
from django.contrib import messages
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    FormView,
)

from braces.views import GroupRequiredMixin

from .constants import CREATE_ACCOUNT_INDICATION
from djangosearchpaginationplus.views import (
    DinamicPaginationMixin, 
    SearchMixin
)
from pagos.models import Calendario
from .forms import CuentaCreateForm
from .models import Cuenta


class CuentaListView(GroupRequiredMixin, DinamicPaginationMixin,
                      SearchMixin, ListView):
    group_required = ('administrador_front', )
    model = Cuenta
    template_name = 'cuentas/list.html'

    def get_filter(self, queryset):
        search = self.get_search()

        if search:
            queryset = queryset.filter(
                Q(grupo__nombre__icontains=search) |
                Q(estatus__icontains=search) |
                Q(monto__icontains=search) |
                Q(saldo__icontains=search) 
            )
        return queryset


class CuentaDetailView(GroupRequiredMixin, DetailView):
    group_required = ('administrador_front', )
    model = Cuenta
    template_name = 'cuentas/detail.html'


class CuentaCreateView(GroupRequiredMixin, CreateView):
    group_required = ('administrador_front')
    form_class = CuentaCreateForm
    template_name = 'core/form.html'
    success_msg = 'Cuenta creada exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        messages.warning(self.request, CREATE_ACCOUNT_INDICATION)
        data = {
            'title': 'Crear Cuenta',
            'button_legend': 'Crear cuenta'
        }
        context.update(data)
        return context

    def form_valid(self, form):
        # create new calendar
        new_calendar = Calendario.crear(
            numero_pagos=form.instance.numero_pagos,
            user_create = self.request.user
        )
        form.instance.calendario = new_calendar
        form.instance.user_create = self.request.user
        form.instance.saldo = form.instance.monto
        messages.success(
            self.request, 
            self.success_msg
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            'cuentas:detalle', 
            kwargs = {'pk': self.object.pk }
        )

