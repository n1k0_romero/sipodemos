# Generated by Django 2.2.12 on 2020-05-24 22:40

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pagos', '0001_initial'),
        ('cuentas', '0006_auto_20200522_1932'),
    ]

    operations = [
        migrations.AddField(
            model_name='cuenta',
            name='calendario',
            field=models.ForeignKey(default=1, help_text='Calendario de pagos', on_delete=django.db.models.deletion.PROTECT, to='pagos.Calendario'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='cuenta',
            name='monto',
            field=models.DecimalField(decimal_places=2, help_text='Monto de la cuenta', max_digits=15, validators=[django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='cuenta',
            name='saldo',
            field=models.DecimalField(decimal_places=2, help_text='Saldo de la cuenta', max_digits=15, validators=[django.core.validators.MinValueValidator(0)]),
        ),
    ]
