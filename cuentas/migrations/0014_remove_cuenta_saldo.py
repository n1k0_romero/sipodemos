# Generated by Django 2.2.12 on 2020-05-27 18:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cuentas', '0013_delete_cuentadocumento'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cuenta',
            name='saldo',
        ),
    ]
