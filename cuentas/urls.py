from django.urls import path
from .views import (
	CuentaListView,
	CuentaDetailView,
	CuentaCreateView,
)


cuentas_patterns = ([
	path('listar/', CuentaListView.as_view(), name='listar'),
	path('<slug:pk>/detalle/', CuentaDetailView.as_view(), name='detalle'),
	path('crear/', CuentaCreateView.as_view(), name='crear'),
], 'cuentas')