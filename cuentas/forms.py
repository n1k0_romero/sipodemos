# -*- coding: utf-8 -*-
from django import forms 

from grupos.models import Grupo
from .models import Cuenta


class CuentaCreateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Cuenta
        fields = [
            'grupo',
            'monto',
            'numero_pagos',
        ]


