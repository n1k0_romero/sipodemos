# -*- coding: utf-8 -*-
from django.core.validators import RegexValidator
from django.db import models
from .regex import(
    REGEX_ACCOUNT_CODE,
    ONLY_ACCOUNT_CODE_MESSAGE,
    CODE_ACCOUNT_CODE 
)


class AccountCodeable(models.Model):
    """
    Clase abstracta que provee el codigo de la cuenta.
    """

    codigo = models.CharField(
        unique=True,
    	max_length=5,
        help_text='Código de indentificación de la cuenta',
        validators=[
        	RegexValidator(
        		regex=REGEX_ACCOUNT_CODE,
        		message=ONLY_ACCOUNT_CODE_MESSAGE,
        		code=CODE_ACCOUNT_CODE
    		)
        ]
    )

    class Meta:
        abstract = True