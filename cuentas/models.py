# -*- coding: utf-8 -*-
import uuid
from datetime import timedelta

from django.core.validators import MinValueValidator
from django.db import (
    models,
    transaction, 
    IntegrityError
)
from django.db.models.signals import post_save
from django.dispatch import receiver

from core.behaviors import BasicTimeStampable
from core.utils import random_with_N_digits
from grupos.models import Grupo
from pagos.models import (
    Calendario,
    PagoIndividual,
)

from .behaviors import AccountCodeable


class Cuenta(BasicTimeStampable, AccountCodeable):
    '''Administra la información de las Cuentas.'''

    ESTATUS_DESEMBOLSADA = 'DESEMBOLSADA'
    ESTATUS_CERRADA = 'CERRADA'

    ESTATUS_OPCIONES = (
    	(ESTATUS_DESEMBOLSADA, 'Cuenta Desembolsada'),
    	(ESTATUS_CERRADA, 'Cuenta Cerrada'),
	)

    NUMERO_DE_PAGOS_DEFAULT = 4

    NUMERO_DE_PAGOS_OPCIONES = [
        (number, number) for number in range(1, 11)
    ]

    id = models.UUIDField(
        primary_key=True, 
        default=uuid.uuid4, 
        editable=False
    )
    grupo = models.ForeignKey(
        Grupo,
        on_delete=models.PROTECT,
        help_text="Grupo al que pertenece la cuenta"
    )
    estatus = models.CharField(
        max_length=12,
        choices=ESTATUS_OPCIONES,
        default=ESTATUS_DESEMBOLSADA,
        help_text="Estatus de la cuenta"
    )
    monto = models.DecimalField(
    	max_digits=15,
    	decimal_places=2,
        validators=[
            MinValueValidator(0)
        ],
        help_text='Monto de la cuenta'
    )    
    calendario = models.ForeignKey(
        Calendario,
        on_delete=models.PROTECT,
        related_name='cuenta',
        help_text="Calendario de pagos"
    )
    numero_pagos = models.PositiveSmallIntegerField(
        choices=NUMERO_DE_PAGOS_OPCIONES,
        default=NUMERO_DE_PAGOS_DEFAULT,
        help_text='Numero de pagos de la cuenta'
    )

    @classmethod
    def generar_codigo_cuenta(cls):
        '''
        Genera un codigo unico de identificación
        constituido por 5 digitos irrepetibles.
        '''

        codigo_cuenta = random_with_N_digits(5)
        while cls.objects.filter(codigo=codigo_cuenta).exists():
            codigo_cuenta = random_with_N_digits(5)
        return codigo_cuenta

    def calcular_monto_total_pagado(self): 
        '''Calcula el monto total de la cuenta.'''
        monto = 0
        pagos_individuales = self.calendario.pagos.all()
        for pago_individual in pagos_individuales:
            for abono in pago_individual.abonos.all():
                monto += abono.monto
        return monto

    @property    
    def puede_cerrarse(self):
        '''Indica si se cubrio el monto total'''
        if self.monto == self.calcular_monto_total_pagado():
            return True
        else:
            return False

    def cerrar(self):
        '''Marca la cuenta con estatus cerrada.'''
        self.estatus = Cuenta.ESTATUS_CERRADA
        self.save()

    def __str__(self):
        return '%s - %s' % (self.grupo, self.estatus)

    class Meta:
         ordering = ['-created']
         verbose_name = "Cuenta"
         verbose_name_plural = "Cuentas"


@receiver(post_save, sender=Cuenta)
def perform_create(sender, instance, created, **kwargs):
    
    if created:
        # generar codigo simple de indentificación
        instance.codigo = Cuenta.generar_codigo_cuenta()
        instance.save()
        
        # crear pagos individuales para el calendario
        periodo_pago = instance.calendario.periodo_pago
        fecha_esperada  = instance.created + timedelta(days=periodo_pago) 
        for _ in range(0, instance.numero_pagos):
            pago_individual = PagoIndividual.crear(
                monto_individual = instance.calendario.monto_individual,
                fecha = fecha_esperada,
                user_create = instance.user_create
            )
            fecha_esperada += timedelta(days=periodo_pago) 
            instance.calendario.pagos.add(pago_individual)


