# -*- coding: utf-8 -*-


REGEX_ACCOUNT_CODE = '^[ 0-9 ]+$'
ONLY_ACCOUNT_CODE_MESSAGE = 'Error... account code are made up of 5 integers'
CODE_ACCOUNT_CODE = 'INT_ACCOUNT_CODE'