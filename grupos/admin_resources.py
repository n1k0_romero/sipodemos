# -*- coding: utf-8 -*-
from core.admin_mixins import ModelResourceSetUser

from .models import Grupo


class GroupInyectorModelResource(ModelResourceSetUser):
    '''
    Recurso para la importación de datos del grupo.
    Formatos soportados:
        -CSV
        -XLS
        -XLSX
        -TSV
        -JSON
        -YAML
    '''

    class Meta:
        model = Grupo