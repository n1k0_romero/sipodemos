# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.test import TestCase

from clientes.models import Cliente
from .models import Grupo


class GrupoTestCase(TestCase):

	def setUp(self):
		'''Inicializa recursos necesarios.'''

		self.test_user1 = User.objects.create_user(
			username='test_user1',
			password='abc123'
		)			
	
		self.test_grupo = Grupo.objects.create(
			user_create = self.test_user1,
			nombre = 'Grupo TEST',
			codigo = '12345'
		)

		self.test_cliente1 = Cliente.objects.create(
			user_create = self.test_user1,
			nombre = 'Cliente TEST 1',
			codigo = '1234567'
		)		

		self.test_cliente2 = Cliente.objects.create(
			user_create = self.test_user1,
			nombre = 'Cliente TEST 2',
			codigo = '765432'
		)

	def test_grupo_user_create(self):
		'''Verifica que el grupo tenga un usuario creador.'''

		user_create_esperado = f'{self.test_grupo.user_create}'
		self.assertEqual(user_create_esperado, 'test_user1')

	def test_grupo_esta_activo(self):
		'''Verifica que el grupo este activo.'''

		activo_esperado = self.test_grupo.is_active
		self.assertEqual(activo_esperado, True)

	def test_grupo_nombre(self):
		'''Verifica el nombre del grupo.'''

		nombre_esperado = f'{self.test_grupo.nombre}'
		self.assertEqual(nombre_esperado, 'Grupo TEST')


	def test_cliente_codigo(self):
		'''Verifica el codigo del grupo.'''

		codigo_esperado = f'{self.test_grupo.codigo}'
		self.assertEqual(codigo_esperado, '12345')	

	def test_agregar_miembros(self):
		'''Verifica que se agregen adecuadamente miembros.'''
		
		self.test_grupo.clientes.add(
			self.test_cliente1,
			self.test_cliente2,
		)
		self.assertEqual(
			self.test_grupo.clientes.all().count(), 
			2
		)
