# -*- coding: utf-8 -*-
from django.contrib import messages
from django.db.models import Q
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
)
from django.urls import reverse_lazy

from braces.views import GroupRequiredMixin

from core.view_mixins import CreateMixin
from djangosearchpaginationplus.views import (
    DinamicPaginationMixin, 
    SearchMixin
)
from .forms import (
    GrupoForm,
    GrupoAgregarMiembroForm,
)
from .models import Grupo


class  GrupoListView(GroupRequiredMixin, DinamicPaginationMixin,
                     SearchMixin, ListView):
    group_required = ('administrador_front', )
    model = Grupo
    template_name = 'grupos/list.html'

    def get_filter(self, queryset):
        search = self.get_search()

        if search:
            queryset = queryset.filter(
                Q(nombre__icontains=search) |
                Q(codigo__icontains=search) |
                Q(clientes__nombre__icontains=search) 
            )
        return queryset


class GrupoDetailView(GroupRequiredMixin, DetailView):
    group_required = ('administrador_front', )
    model = Grupo
    template_name = 'grupos/detail.html'


class GrupoCreateView(CreateMixin, CreateView):
    form_class = GrupoForm
    template_name = 'core/form.html'
    title = 'Crear Grupo'
    button_legend = 'Crear Nuevo Grupo'
    success_msg = 'Grupo creado con exito'



class GrupoAgregarMiembroView(CreateMixin, UpdateView):
    model = Grupo
    form_class = GrupoAgregarMiembroForm
    template_name = 'core/form.html'
    title = 'Agregar Cliente'
    button_legend = 'Agregar cliente a grupo'
    success_msg = 'Cliente exitosamente agregado a grupo'

