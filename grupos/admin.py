# -*- coding: utf-8 -*-
from django.contrib import admin

from import_export.admin import ImportExportMixin

from .models import Grupo
from .admin_resources import GroupInyectorModelResource


@admin.register(Grupo)
class GrupoAdmin(ImportExportMixin, admin.ModelAdmin):
    '''
    Configuración Django admin e importación de datos.
    Formatos soportados:
        -CSV
        -XLS
        -XLSX
        -TSV
        -JSON
        -YAML
    '''

    resource_class = GroupInyectorModelResource
    search_fields = (
        'codigo',
        'nombre'
    )
    readonly_fields = (
        'created',
        'modified'
    )
    list_display = (
        'codigo', 
        'nombre', 
        'created',
        'user_create', 
        'is_active',
    )
