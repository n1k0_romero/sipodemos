# -*- coding: utf-8 -*-
import uuid

from django.db import models
from django.urls import reverse

from clientes.models import Cliente
from core.behaviors import (
    BasicTimeStampable,
    Nameable,
)

from .behaviors import GrupCodeable


class Grupo(BasicTimeStampable, Nameable, GrupCodeable):
    '''Administra la información de los grupos.'''

    id = models.UUIDField(
        primary_key=True, 
        default=uuid.uuid4, 
        editable=False
    )
    clientes = models.ManyToManyField(
        Cliente,
        related_name='grupos',
        help_text="Ctrl mantenido mas click en cliente"
    )

    def __str__(self):
        return '%s - %s' % (self.codigo, self.nombre)

    def get_absolute_url(self):
        return reverse("grupos:detalle", kwargs={"pk": self.pk})

    class Meta:
         ordering = ['nombre']
         verbose_name = "Grupo"
         verbose_name_plural = "grupos"