from django.urls import path, include

from .views import(
	GrupoListView,
	GrupoDetailView,
	GrupoCreateView,
	GrupoAgregarMiembroView,
)


grupos_patterns = ([
	path('listar/', GrupoListView.as_view(), name='listar'),
	path('<slug:pk>/detalle/', GrupoDetailView.as_view(), name='detalle'),
	path('crear/', GrupoCreateView.as_view(), name='crear'),
	path('<slug:pk>/agregar/miembro/', GrupoAgregarMiembroView.as_view(), name='agregar_miembro'),
], 'grupos')