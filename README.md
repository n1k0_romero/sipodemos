# N1k0 

Proyecto Prueba.

  - Clientes
  - Grupos
  - Cuentas
  - Pagos
  
### Tecnologia
    - Python 3
    - Django 2.2.12 LTS
    - MysQl
 
### Installation

Clonar proyecto.
```sh
$ git clone git@gitlab.com:n1k0_romero/sipodemos.git
```

Crear entorno virtual.
```sh
$ mkvirtualenv -p Python3 sipodemos
```

Instalar dependencias.
```sh
$ cd sipodemos/requirements
$ pip install -r development.txt
```

Correr.
```sh
$ Python manage.py runserver
```